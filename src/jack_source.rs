use std::{sync, thread, time};

type Sample = f32;

type BoxedProcessCallback =
    Box<dyn 'static + Send + FnMut(&jack::Client, &jack::ProcessScope) -> jack::Control>;

#[derive(Debug)]
pub struct JackSource {
    _client: jack::AsyncClient<(), jack::ClosureProcessHandler<BoxedProcessCallback>>,
    queue: sync::Arc<crossbeam::queue::ArrayQueue<Sample>>,
    sample_rate: usize,
    iterator_sleep_delay: time::Duration,
}

const QUEUE_CAP_MULTIPLIER: usize = 16;

impl JackSource {
    pub fn new(name: &str) -> Self {
        let (client, _status) =
            jack::Client::new(name, jack::ClientOptions::NO_START_SERVER).unwrap();
        let sample_rate = client.sample_rate();
        let iterator_sleep_delay =
            time::Duration::from_secs_f64(client.buffer_size() as f64 / sample_rate as f64);

        let input_port = client
            .register_port("input", jack::AudioIn::default())
            .unwrap();
        let queue = sync::Arc::new(crossbeam::queue::ArrayQueue::<Sample>::new(
            QUEUE_CAP_MULTIPLIER * client.buffer_size() as usize,
        ));
        let queue2 = queue.clone();
        let process_callback: BoxedProcessCallback = Box::new(
            move |_: &jack::Client, ps: &jack::ProcessScope| -> jack::Control {
                let buf = input_port.as_slice(ps);
                for sample in buf {
                    if queue2.push(*sample).is_err() {
                        println!("buffer overrun");
                        break;
                    }
                }
                jack::Control::Continue
            },
        );
        let process = jack::ClosureProcessHandler::new(process_callback);

        let active_client = client.activate_async((), process).unwrap();

        Self {
            _client: active_client,
            queue,
            sample_rate,
            iterator_sleep_delay,
        }
    }

    pub fn sample_rate(&self) -> usize {
        self.sample_rate
    }
}

impl Iterator for &JackSource {
    type Item = Sample;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self.queue.pop() {
                Some(sample) => return Some(sample),
                None => {
                    thread::sleep(self.iterator_sleep_delay);
                }
            }
        }
    }
}
