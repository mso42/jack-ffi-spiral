use std::sync;

type Sample = f32;

pub struct FftProcessor {
    scratch: Box<[num_complex::Complex<Sample>]>,
    working_buf: Vec<num_complex::Complex<Sample>>,
    fft: sync::Arc<dyn rustfft::Fft<Sample>>,
}

impl FftProcessor {
    pub fn new(fft_size: usize) -> Self {
        let fft = rustfft::FftPlanner::<Sample>::new().plan_fft_forward(fft_size);
        let scratch = vec![num_complex::Complex::new(0.0, 0.0); fft.get_inplace_scratch_len()]
            .into_boxed_slice();
        let buf = Vec::with_capacity(fft_size);
        Self {
            fft,
            scratch,
            working_buf: buf,
        }
    }

    pub fn process_magnitude(&mut self, input_buf: &mut [Sample]) {
        self.working_buf.truncate(0);
        for sample in input_buf.iter() {
            self.working_buf
                .push(num_complex::Complex::new(*sample, 0.0));
        }
        self.fft
            .process_with_scratch(&mut self.working_buf, &mut self.scratch);
        for (norm, out) in self
            .working_buf
            .iter()
            .map(|c| c.norm())
            .zip(input_buf.iter_mut())
        {
            *out = norm;
        }
    }
}
