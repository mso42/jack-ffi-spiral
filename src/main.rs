mod fft;
mod jack_source;

use fft::FftProcessor;
use graphics::polygon::SpiralPlot;
use jack_source::JackSource;

fn main() {
    let source = JackSource::new("ffi_spirals");
    let fft_size = (0.1 * source.sample_rate() as f64) as usize;
    let mut buf = Vec::with_capacity(fft_size);
    let mut fft_processor = FftProcessor::new(fft_size);
    let mut spiral_plotter = SpiralPlot::new(fft_size, 1.0, 0.01);
    for sample in &source {
        buf.push(sample);
        if buf.len() == buf.capacity() {
            fft_processor.process_magnitude(buf.as_mut_slice());
            let spiral_polygon = spiral_plotter.plot_spiral(buf.as_slice());
            println!("{:?}", buf[0]);
            println!("{:?}", spiral_polygon[0]);
            buf.truncate(0);
        }
    }
}

mod graphics {
    pub mod polygon {
        use std::f32::consts::PI;

        pub type Point = (f32, f32);
        pub type PolygonSlice = [Point];

        pub struct SpiralPlot {
            index_scale_factor: f32,
            width_factor: f32,
            len: usize,
            out: Box<PolygonSlice>,
        }

        impl SpiralPlot {
            pub fn new(len: usize, loops: f32, width_factor: f32) -> Self {
                let index_scale_factor = loops * 2.0 * PI / len as f32;
                let polygon = vec![(0.0, 0.0); 2 * len].into_boxed_slice();
                Self {
                    index_scale_factor,
                    width_factor,
                    len,
                    out: polygon,
                }
            }

            pub fn plot_spiral<'this>(&'this mut self, values: &[f32]) -> &'this PolygonSlice {
                debug_assert!(values.len() == self.len);
                for (index, value) in values.iter().enumerate() {
                    let theta = self.index_scale_factor * index as f32;
                    let r1 = theta / (2.0 * PI) - self.width_factor * value;
                    let r2 = theta / (2.0 * PI) + self.width_factor * value;
                    let x1 = r1 * theta.cos();
                    let y1 = r1 * theta.sin();
                    let x2 = r2 * theta.cos();
                    let y2 = r2 * theta.sin();
                    self.out[index] = (x1, y1);
                    self.out[index + self.len] = (x2, y2);
                }
                &self.out
            }
        }
    }
}
